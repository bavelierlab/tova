<!--- [![DOI](https://sandbox.zenodo.org/badge/633319443.svg)](https://sandbox.zenodo.org/badge/latestdoi/633319443) --->

<p align="center">
  <a href="https://youtu.be/IyfIl-8L1DI?feature=shared"><img src="assets/img/stim_up.png"></a>
</p>

Click on the image to see a demo video. NOTE: The image stimulus has changed since then, it is now a white square with a smaller blue square inside (either on a top position, or a bottom position).

# Table of contents

- [Description](#-description)
- [Online demo](#-online-demo)
- [Demo video](#%EF%B8%8F-demo-video)
- [Usage](#%EF%B8%8F-how-to-use-it)
- [Data dictionary](#-data-dictionary)
- [What could differ from ACE-X](#%EF%B8%8F-what-could-differ-from-ace-x)
- [Citation](#-citation)
- [References](#-references)

---

# 📝 Description

When using this task, you can copy-paste this in your Methods' section of your manuscript :

> This task has been adapted by the Bavelierlab (Rioja & Denkinger, 2023) based on a Go/NoGo task as implemented in the ACE-X battery (https://neuroscape.ucsf.edu/researchers-ace/). It is a target detection task where participants are instructed to respond when a blue square appears at the top of the screen (Go) and withhold their response when is it presented at the bottom of the screen (NoGo). To familiarize with the task, a practice session of five trials (no-go, go, go, no-go, go) is first presented. The rule to pass the practice session is to have at least 3 correct trials and a maximum of 3 practice blocks is allowed. Feedback is given after each practice trial with the fixation cross becoming green (correct response) or red (incorrect response).<br>
The task is then divided into 2 blocks: the first block is measuring sustained attention (SA) where response is required on infrequent targets (20% of Go trials, 80% of No-go trials, 160 trials); the second block measures inhibition control (IC) where response is required to frequent targets, while withholding response to the remaining infrequent targets (80% of go trials, 20% of no-go trials, 80 trials).<br>
The blocks are always presented in the same order: first SA, then IC. Percentage of accuracy for hits and correct rejections is only given at the end of each block. 
White square width is set at five visual angle and blue square width at one visual angle at 50cm eyes-screen. Each stimulus is flashed during 200 ms (presentation time). Stimulus Onset Asynchrony (SOA) is defined at 2000 ms ; for trials with reaction times (RT) strictly inferior than 600 ms (response window), SOA is set at : RT + 900 ms. RT and trial accuracy (hit, false alarm, miss, correct rejection) are saved throughout the task.


# 📈 Online demo

Clicke [here](https://brainandlearning.org/tasks/tova/) to test the task itself (test version). At the end of the task, a popup window will ask you to download the data.

# ▶️ Demo video (Deprecated)

Click [here](https://youtu.be/IyfIl-8L1DI?feature=shared) to see a video of the task (v.1.0).

# Usage

 ## URL parameters

`subject_id`, `study_id`, `session_id`, `monitor_size_inch`, if you put them in URL, e.g., mywebsite.org/?subject_id=johndoe, you will automatically get subject_id and skip the question about it

## Local

- Clone this repo or download the zip (+ unzip it).

- Use an [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) ! [Visual Studio Code (VSC)](https://code.visualstudio.com/) is the way to go.

- Once you have downloaded it, you can open the cloned/downloaded repo/folder in VSC.

- Open the index.html file on VSC.

- Make sure you have *Live Server* VSC Extension installed, you must have a "Go Live" on the bottom right of the screen (Keyboard shortcut should be Cmd+L followed by Cmd+O) – if you don't download the extension.

- "Go Live" and you are ready to test the task!

## Production

1. Fork the repo

2. In src/code.js, change savedb to true, change showcsvbutton to true, change redirectURL and redirectText

3. At the root of the repo, `cp .envcopy .env`, and change values to the one corresponding to your db

4. rsync the repo to your server: `rsync -avPr --exclude .git -e 'ssh -i /path/to/pem' /path/to/your/file username@host:/path/to/your/project/on/ec2`

5. In your MySQL database, copy-paste the sql query found under db/db-structure.sql, you must now have a tova table ready to be populated

6. Go to your task online and it should now work!

# 📖 Data dictionary

Click [here](/assets/refs/datadictionnary_tova.txt) to view the description of the collected variables.


# ⚡️ What could differ from ACE-X

- **We did not find the 25/75% ratio :** <br>
*Its is not an exact ratio since it is randomly generated + checks to not have 3 consecutive target trials, this will make the ratio vary. The trial ratio is randomly generated and will not always give the expected ratio (25%/75%) but can sometimes vary and give 20%/80%, 23%/77%, or 23%/77%, but this happens because of the nature of a random generator.*<br>
Re-checking our data (6 ss, each 120 trials) and ratio were as follow:
  - only for the test trials: ratio was always = 20/80%.
  - if practice trials are added: + 5 trials = 21.17/78.83%,+ 10 trials = 22.22/77.78%,+ 15 trials = 23.15/76.85%). The test allows for a maximum of 3 practice rounds (each repeating the same sequence of 5 trials) then proceed to the test. <br>
  - Results are not congruent with what we were told.

  **→ We decided to go for 20/80% for test trials, as it best fit our data and the data in the report sent. Then we added 5 practice trials (using the same array for each participant: [0,1,1,0,1])**.

- **Time between 2 stimulus presentation do not correspond to given times (we found ~2000ms more for each trials):**<br>
*The order of the task is as it follows:*
  - *Game starts*
  - *Trials get generated*
  - *The current trial starts*
  - *It waits either for the user's input or for the responseWindowCountdown to finish (which is the maxResponseTime of 3100ms)*
  - *Once one of the above conditions is completed, the trial info is registered.*
  - *Then it waits for the interTrialInterval (which is the one in the report with the same name with a value of 2000ms)*
  - *After that if the are trials left, it repeats until there are no more.*

  According to what's above, the noGo trials would be equal to 3100ms (responseWindow) + 2000ms (ITI) = total time of 5100ms, this again do not correspond with what we measured (4095ms).....<br>
  **→ We calculated the SOA between a NoGo trial followed by a Go trial (which should correspond to the highest possible SOA) and found precisely the 4095ms we measured when doing the ACE-X task. Therefore, we decided to go for a 4100ms SOA.**

  For interval after Go trials ((either Hit or False Alarm), either the response Window (= our SOA) or  take the RT of the participant and add the interTrialInterval of 2100ms.<br>
  For NoGo trials, we allowed a response window = to the SOA. As the info from ACE were not clear, this will allow to save all key press from ss and will permit to see if some anticipatory responses are made (i.e. when the ss will press the space bar just before the next stimulus appears). For data cleaning, we might consider to exclude responses made after 2000ms (to decide later)<br>
  **→ We decided to have a modular SOA (for each trial type) when response was given before 2000ms leading to SOA(RT<=2000) = RT + 2100 and SOA(RT>2000) = 4100**

# 🛟 Citation 

## APA : 

```
Rioja, K., & Denkinger, S. (2023). Bavelierlab adapted ACE-X Test of Variables of Attention (Version 1.1) [Computer software]. https://doi.org/10.5072/zenodo.1234569
```

## BibTeX :

```
@software{Rioja_Bavelierlab_adapted_ACE-X_2023,
author = {Rioja, Kenneth and Denkinger, Sylvie},
doi = {10.5072/zenodo.1234569},
month = aug,
title = {{Bavelierlab adapted ACE-X Test of Variables of Attention}},
url = {https://github.com/kennethrioja/bl_tova/tree/v1.0},
version = {1.1},
year = {2023}
}
```

# 🔭 References

de Leeuw, J.R. (2015). jsPsych: A JavaScript library for creating behavioral experiments in a Web browser. Behavior Research Methods, 47(1), 1-12. [doi:10.3758/s13428-014-0458-y](https://doi.org/10.3758/s13428-014-0458-y)
