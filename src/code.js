// TITLE : Bavelier Lab 'Task of Variable Attention'
// VERSION : 1.1 (deploy now should be ok)
// AUTHOR : Kenneth Rioja
// EMAIL : kenneth.rioja@unige.ch
// DATE : 05.02.2025
// GITHUB LINK : https://gitlab.unige.ch/bavelierlab/tova
// NOTE FOR RESEARCHERS AND DEVS : Please carefully read the whole section below with global variables + the data dictionnary. If you have any comments, please send me an email. I would love to read from someone who got into this code ! Thanks and have fun ! 
// NOTE FOR THE PERSON IN CHARGE OF THE BACKEND IMPLEMENTATION : I notified relevant points for you though comments having 'BACKEND' inside them. Please read them before implementing this code.

// #######################
// ### input variables ###
// #######################

// 1. To get from URL or other method, PP_ID, STUDY_ID, SESSION_ID, MONITOR_SIZE_INCH to get from url, see get_subject_id variable – if not using it then change global variable ask_for_id to false

// ############################
// ### experiment variables ###
// ############################
// based on Denkinger Sylvie's 'TOVA_parameters_2023' excel sheet 

const   pres_time = 200; // stimulus presentation time in ms
const   soa = 2000; // duration in ms between the onset of two consecutive stimuli. Note : In the original ACE-X TOVA, they have a 2000ms response-window. To be alligned with them, when analysing data, be sure that responses after 2000ms are not counted and treated as anticipatory responses.
const	resp_window = 600; // this will determine the buffer_time, if you answer anytime before the end of the resp_window, it will activate the buffer_time, e.g., soa=4100, resp_window=2000, buffer_time=2100, it means that if you answer before 2000ms, the trial will wait 2100ms before going to the next trial ELSE next trial will be 4100ms after the begining of the trial. NOTE : it will only affect the aesthetic display of the trials, not the outcome in terms of FA, correct, etc...
const	buffer_time = 900; // buffer_time, time to wait after the next trial when response was given before the end of 'resp_window'. NOTE : it will only affect the aesthetic display of the trials, not the outcome in terms of FA, correct, etc...
const   stim_diag_va = 5; // https://elvers.us/perception/visualAngle/

// const isi = soa - pres_time; // inter stimulus interval, NOT IN-USE IN THE CODE
const   root_path = './'; // BACKEND TO MODIFY IF NEEDED
// const   root_path = 'https://s3.amazonaws.com/BavLab/TOVA/'; // BACKEND TO MODIFY IF NEEDED
const   fixation_cross = `
                            <div class='fixcross' id='cross'>+</div>
                            `; // to change its size, see 'assets/css/style.css'
const   practice_array = [0,1,1,0,1]; // 1 for go, 0 for no go
const   block_type = ['SA', 'IC']; // fixed order, sustained attention then inhibitory control. Note : those are the names under the column 'block', they are not used for functional code - only aesthetical
// const   fixed_80_block_01_sa = [0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0]; // fixed 80 SA (20% go / 80% nogo) block was computed through this function : lines 510-550 from https://github.com/kennethrioja/bl_tova/commit/64cf61b942450fd5e397914d371c191e109310e6 - it can create infinite loops, my bad
// const   fixed_40_block_02_ic = [1,1,0,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,0,1,1,1,1,1,0,0,1,1,1,1,0,1,1,1,1]; // fixed 40 IC (80% go / 20% nogo) block was computed through this : lines 510-550 from https://github.com/kennethrioja/bl_tova/commit/2508d50e70703f6e706a744ed47b424fac08ff6e - it can create infinite loops, my bad
// const   fixed_blocks_array = [fixed_80_block_01_sa, fixed_40_block_02_ic] // this is the  array on which the code is based
const   fixed_160_block_01_sa = [0,1,0,1,0,0,0,0,1,0,0,0,0,1,0,0,1,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0]; // fixed 160 SA (20% go / 80% nogo) block was computed through this function : lines 510-550 from https://github.com/kennethrioja/bl_tova/commit/64cf61b942450fd5e397914d371c191e109310e6 - it can create infinite loops, my bad
const   fixed_80_block_02_ic = [1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0]; // fixed 80 IC (80% go / 20% nogo) block was computed through this : lines 510-550 from https://github.com/kennethrioja/bl_tova/commit/2508d50e70703f6e706a744ed47b424fac08ff6e - it can create infinite loops, my bad
const   fixed_blocks_array = [fixed_160_block_01_sa, fixed_80_block_02_ic] // this is the  array on which the code is based
const   post_instructions_time = 2000; // time to wait after instruction to begin the trials

// #######################
// ### task monitoring ###
// #######################

const   feedback_color_array = [true, false]; // manually set this here. true = your fixation cross become green for correct answers or red for incorrect, for no-go trials the fixation cross becomes green 250ms before the end of the trial. First one is for practice, second is for main task
var     feedback_color = false; // true = changes fixation cross to green/red depending of correct/incorrect response at the end of each trial, see plugin-html-keyboard-response.js. This global variable will be updated (var not const) depending on feedback_color_array and is needed to communicate with plugin-html-keyboard-response.js.
const   ask_for_id = true; // manually set this here. true = displays a form asking for subject id, study id and session id. BACKEND : set to false when you can retreive the following variables, for example though the URL. The latter MUST CONTAIN '?PP_ID=*&STUDY_ID=*&SESSION_ID=*' with '*' being the corresponding values to variables.
var     do_practice = true; // manually set this here. true = do practice, false = don't. BACKEND : can be a way to skip practice if problem during task, that's why it is 'var' and not 'const'.
var     repeat_practice = []; // array containing either 1 (= practice must be repeated), or 0 (= practice is not repeated). Relevent when do_practice = true, it stores whether the participant needs to repeat the practice or not. Rule : if 3 correct responses over 5 then pass, otherwise repeat and allow 2 repetitions then pass anyway. See repeat_prac_conditional to see ending loop.
var     testquestion = true; // true = shows the question about the whole experiment or the test trials only
var     testmode = false; // FOR SHOWING PURPOSE ONLY, it gets updated by user if testquestion == true, if true, you go through only 4 trials, if false you go through the entire SA and IC trials
var     savedb = false; // true == enable saving to the db
var     showcsvbutton = true;   // true == at the end of the experiment, a download csv button is here and you can download the data locally
                                // false == redirect to url below with the following text
const   redirectURL = "";
const   redirectText = "";

// strings templates
function showHideDiv(hide, show) { // see review_str
    const srcShow = document.getElementById(show);
    const srcHide = document.getElementById(hide);
    if (srcShow != null) {
        if (srcShow.style.display == 'block') {
            srcShow.style.display = 'none';
            srcHide.style.display = 'block';
        }
        else {
            srcShow.style.display = 'block';
            srcHide.style.display = 'none';
        }
        return false;
    }
};
const   welcome_str = `
                    <p>Please enter in full screen mode, for Windows press 'F11', for MacOS press 'control' + 'command' + 'F' at the same time.</p>
                    <p>Click the button below to continue.<br></p>
                    <p>(Version 1.1, October 2024)</p>
                    `;
const   endblock_practice_str1 = `
                                <p>Well done !</p>
                                <p>% of correctly answering to TOP blue square = 
                                `;
const   endblock_practice_str2 = `
                                %</p>
                                <p>% of correctly refraining answers to BOTTOM blue square = 
                                `;
const   endblock_practice_str3 = `
                                %</p>
                                <p>Press the spacebar to continue.</p>
                                `;
const   endblock_practice_str4 = `
                                <p>You will go through another practice block. Please be as accurate as possible.</p>
                                <p>If the blue square is presented at the TOP, please press the spacebar.</p>
                                <p>If the blue square is presented at the BOTTOM, don't press the spacebar.</p>
                                <p>Press the spacebar to continue.<p>
                                `;
const   review_str = `
                        <div id='review' style='display:block;'>
                        <p>You are ready for the task now.</p>
                        <p>Please press "Instructions" if you want a refresher, otherwise "Begin Task".</p>
                        <p>From now on, we ask you to carry on with the task without the feedback cross</p>
                        <input type='button' value='Instructions' class='jspsych-btn' style="color:grey; background-color:#303030" onClick="showHideDiv('review', 'refresher')"/>
                        </div>
                        <div id='refresher' style='display:none;'>
                        <p>If the blue square is presented at the TOP, please press the spacebar.</p>
                        <p>If the blue square is presented at the BOTTOM, don't press the spacebar.</p>
                        </div>
                        <br>
                        `;
const   endblock_str1 = `
                        <p>Well done !</p>
                        <p>% of correctly answering to TOP blue square = 
                        `;
const   endblock_str2 = `
                        %</p>
                        <p>% of correctly refraining answers to BOTTOM blue square = 
                        `;
const   endblock_str3 = `
                        %</p>
                        <p>Press Enter to start the next block.</p>
                        `;
const   endblock_str4 = `
                        %</p>
                        <p>Press Enter to complete the task.</p>
                        `;
// ending strings : see the end of the code to choose the one you want
const   completion_code_str =`
                            <br><br><br><br><br>
                            <p style='text-align:center'>This is the end of the task.<br><br>
                            Thank you for your participation.<br><br>
                            You can now go back to Prolific and enter the following completion code : <br><br>
                            <strong>XXXX</strong>
                            </p>
                            `;
const   inlab_final_str =`
                            <br><br><br><br><br>
                            <p style='text-align:center'>This is the end of the task.<br><br>
                            Thank you for your participation.<br><br>
                            You can call the experimenter.
                            </p>`;
const   classic_end_str =`
                            This is the end of the task.<br><br>
                            Thank you for your participation.
                            `;


// ########################
// ### custom functions ###
// ########################

/**
 * This function retrieves the current date and time, formats it into 
 * the YYYY-MM-DDTHH:MM:SS.LLLZ, and returns it.
 * 
 * Format:
 * - Date: YYYY-MM-DD (Year-Month-Day)
 * - Time: HH:MM:SS.LLL (Hour:Minute:Second.Millisecond, 24-hour format)
 * 
 * Usage: Call this function whenever you want to log the current timestamp.
 * 
 * @returns 2024-10-03T14:25:07.000Z
 */
function logCurrentDateTime() {
    const now = new Date();
    
    // Format date as YYYY-MM-DD
    const date = now.getFullYear() + '-' + 
                String(now.getMonth() + 1).padStart(2, '0') + '-' + 
                String(now.getDate()).padStart(2, '0');

    // Format time as HH:MM:SS
    const time = String(now.getHours()).padStart(2, '0') + ':' + 
                String(now.getMinutes()).padStart(2, '0') + ':' + 
                String(now.getSeconds()).padStart(2, '0') + '.' +
                String(now.getMilliseconds()).padStart(2, '0');

    // Combine date and time
    const formattedDateTime = `${date}T${time}Z`;
    
    return(formattedDateTime);
};

/**
 * stim_width_px getter
 * 
 * @returns stimulus jsPsych.data.get().last(1).values()[0].stimulus_width_px
 */
function getStimWidthPx(jsPsych) {
    return (jsPsych.data.get().last(1).values()[0].stimulus_width_px);
}

/**
 * Stimulus 'Tova up' getter
 * 
 * @returns A div containing stim_up.png at width getStimWidthPx
 */
function    getTovaUp(jsPsych) {
    return(`<div class='up' id='shape'><img src='${root_path}assets/img/stim_up.png' style='width:${getStimWidthPx(jsPsych)}px'></img></div>`); // id='shape' is mandatory, without it it won't work, see plugin-html-keyboard-response.js
}
/**
 * Stimulus 'Tova down' getter
 * 
 * @returns A div containing stim_down.png at width getStimWidthPx
 */
function    getTovaDown(jsPsych) {
    return(`<div class='down' id='shape'><img src='${root_path}assets/img/stim_down.png' style='width:${getStimWidthPx(jsPsych)}px'></img></div>`); // id='shape' is mandatory, without it it won't work, see plugin-html-keyboard-response.js
}

/**
 * 
 * @returns an array of two objects with tova down and tova up
 */
function    createTovaTimelineVariables(jsPsych) {
    return([
        { // represents 0 in practice_array
            stimulus: getTovaDown(jsPsych),
            stimulus_label: 'shapedown',
            position_is_top: 0,
            trial_condition: 'NoGo'
        },
        { // represents 1 in practice_array
            stimulus: getTovaUp(jsPsych),
            stimulus_label: 'shapeup',
            position_is_top: 1,
            trial_condition: 'Go'
        }
    ]);
};

/**
 * 
 * @param {String} block either 'practice' or 'main' 
 * @param {Number} i most relevant when 'main', 0 == SA, 1 == IC
 * @returns timeline for createTovaBlock
 */
function    createTovaTimeline(jsPsych, block, i) {
    return({
        type: jsPsychHtmlKeyboardResponse, // this records RT from the begining of the stim onset, see '../vendor/plugin-html-keyboard-response.js'
        stimulus: jsPsych.timelineVariable('stimulus'), // this will show the 'stimulus'
        choices: [' '], // this is the array of choices
        stimulus_duration: pres_time, // this is the stimulus presentation
        trial_duration: soa, // this is the soa
        response_ends_trial: false, // false means when a response is done, the trial is not stopping
        prompt: function () { // this show the fixation cross all along
            if (block == 'practice')
                return (fixation_cross);
        },
        data: {
            block: '',
            trial_condition: jsPsych.timelineVariable('trial_condition'),
            position_is_top: jsPsych.timelineVariable('position_is_top'),
            effective_response: '', // will be modified at the end of each trial, see 'on_finish' below
        },
        on_finish: function (data) {
            // give data.block the right label
            data.block = block == 'practice' ? block : block_type[i];
            // give to data.stimulus the right label
            data.stimulus = jsPsych.timelineVariable('stimulus_label');
            // read data.response (= array of key) to give the right number to effective_response
            if (data.response.length >= 2) {
                data.effective_response = 2; // 2 = multiple responses
            } else {
                if (data.response[0]) {
                    data.effective_response = 1; // 1 = pressed space
                } else {
                    data.effective_response = 0; // 0 = refrained
                }
            }
            // compute correct
            data.correct = +(data.position_is_top == data.effective_response);
            // compute trial_accuracy (whether hit, fa, cr, miss)
            data.trial_accuracy = 
                                    +(data.position_is_top && data.effective_response) == 1 ? "Hit" : // is hit ? true = "Hit" otherwise
                                    +(!data.position_is_top && data.effective_response) == 1 ? "False Alarm" : // is fa ? true = "False Alarm" otherwise
                                    +(!data.position_is_top && !data.effective_response) == 1 ? "Correct Rejection" : // is cr ? true = "False Alarm" otherwise
                                    +(data.position_is_top && !data.effective_response) == 1 ? "Miss" : null; // is miss ? true = "Miss" otherwise null
            // modify soa value in data to RT + 2100ms. Note that this is only the value in your data, not what the code behind is currently doing, see plugin-html-keyboard-reponse.js for implementation
            if (jsPsych.data.get().last(1).values()[0].rt) {
                data.soa = +(jsPsych.data.get().last(1).values()[0].rt.split(',')[0]) + 2100;
            }
        }
    });
}

/**
 * 
 * @param {String} block 'practice' or 'main'
 * @param {Number} i when 'main', 0 == SA, 1 == IC
 * @returns block for timeline
 */
function    createTovaBlock(jsPsych, block, i) {
    return({
        timeline_variables: createTovaTimelineVariables(jsPsych),
        timeline: [createTovaTimeline(jsPsych, block, i)], // needs to be an array
        on_timeline_start: function () {
            block == 'practice' ? feedback_color = true : feedback_color = false; // global variable that will be set for each block. True = colored fixation cross depending on correct/incorrect at the end of each trial, see plugin-html-keyboard-response.js. 
        },
        sample: {
            type: 'custom',
            fn: function () {
                if (block == 'practice')
                    return practice_array;
                else {
                    if (testmode)
                        return [0,1,1,0]; // for testing/debuggin purposes 
                    return fixed_blocks_array[i];
                }
            }
        },
        on_timeline_finish: function() {
            jsPsych.pauseExperiment();
            setTimeout(jsPsych.resumeExperiment, post_instructions_time);
        }
    });
}

/**
 * 
 * @param {jsPsych} jsPsych 
 * @returns The Practice conditional
 */

function    createPracticeConditional(jsPsych) {
    return ({
        timeline: [{ // https://www.youtube.com/watch?v=LP7o0iAALik
            type: jsPsychHtmlKeyboardResponse,
            choices: [' '],
            stimulus: endblock_practice_str4,
            on_start: function () {
                document.body.style.backgroundColor = '#202020'; // back to grey
                document.body.style.cursor = 'auto'; // display cursor during instructions
            },
            on_finish: function () {
                document.body.style.backgroundColor = '#000000'; // back to black
                document.body.style.cursor = 'none'; // hide cursor during task
                jsPsych.pauseExperiment();
                setTimeout(jsPsych.resumeExperiment, post_instructions_time);
            }
        }],
        conditional_function: function() {
            const trials_practice = jsPsych.data.get().filter({ block: 'practice' }).last(5); // get last 5 practice trials
            const n_correct_trials_practice = trials_practice.filter({ correct: 1 }).count(); // count only the true last 5 practice trials
            if (n_correct_trials_practice < 3 && repeat_practice.length < 2) {// if not more than 3 correct trials and we allow 2 repetitions , repeat_prac_conditional = true, meaning practice is repeated.
                repeat_practice.push(true);
                return true; 
            } else {
                repeat_practice.push(false);
                return false;
            }
        }
    });
}

// #####################################
// ### modifications in plugin files ###
// #####################################
// see comments '*MODIFIED*' in the plugin files

// 1) allow the recording of multiple responses during one trial
// https://github.com/jspsych/jsPsych/discussions/1302
// file : plugin-html-keyboard-response.js

// 2) if feedback_color = true, the CSS of the fixation cross changes to green or red depending on true or false response on go and no-go trials
// file : plugin-html-keyboard-response.js

// 3) for stimulus, I changed `default: undefined` to `default: ""` to avoid having a 'undefined' word at the end of a block
// file : plugin-html-keyboard-response.js

// 4) in final csv, creation of 'real_trial_index' column which is the trial_index for each block, 0 if not a real trial, begin by 1 if block
// file : jspsych.js

// 5) grey previous buttons in instructions
// file : plugin-instructions.js

// 6) when a response is given, the trial ends 2100ms after reponse. SOA(RT<2000ms) = RT + 2100ms, data.soa is changed in this file, see 'data.soa'
// file : plugin-html-keyboard-response.js

// ###################
// ### precautions ###
// ###################

// prevent ctrl+r or cmd+r : https://stackoverflow.com/questions/46882116/javascript-prevent-page-refresh-via-ctrl-and-r, https://stackoverflow.com/questions/3902635/how-does-one-capture-a-macs-command-key-via-javascript
$(document).ready(function () {
    $(document).on('keydown', function(e) {
        e = e || window.event;
        if (e.ctrlKey || e.metaKey) {
            let c = e.which || e.keyCode;
            if (c == 82) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    });
});

// prevent getting back or closing the window : https://stackoverflow.com/questions/12381563/how-can-i-stop-the-browser-back-button-using-javascript
window.onbeforeunload = function() { return `Your work will be lost.`; };

const tova = async() => {

    // ##########################
    // ### initialize jsPsych ###
    // ##########################

    var jsPsych = initJsPsych();

    // create the timeline
    var pretask = [];
    var timeline = [];

   // preload the images
    pretask.push({
        type: jsPsychPreload,
        images: [
            root_path + 'assets/img/tova_up.png',
            root_path + 'assets/img/tova_down.png'
        ],
        on_start: function() {
            document.body.style.backgroundColor = '#FFFFFF'; // white background
            document.body.style.color = '#000000'; // black typo
            jsPsych.data.addProperties({
                log_start: logCurrentDateTime(),
            });
        }
    });

    // get browser data
    pretask.push({ 
        type: jsPsychBrowserCheck, // allows to have data on screen width, heigth, browser used, see https://www.jspsych.org/7.2/plugins/browser-check/
        data: {
            block: 'practice' // for export filtering purpose, see final at the end
        },
        skip_features: ['width', 'height', 'webaudio', 'webcam', 'microphone'],
    });

    // input variables to get from url
    const get_subject_id = jsPsych.data.getURLVariable('subject_id');
    const get_study_id = jsPsych.data.getURLVariable('study_id');
    const get_session_id = jsPsych.data.getURLVariable('session_id');
    const get_monitor_size_inch = jsPsych.data.getURLVariable('monitor_size_inch');

    // directly add properties if global var ask_for_id is false OR if we miss monitor_size_inch, we should ask for it
    if (!ask_for_id && typeof get_monitor_size_inch !== 'undefined') { // TO TEST
        // add variables to data
        jsPsych.data.addProperties({
            subject_id: get_subject_id,
            study_id: get_study_id,
            session_id: get_session_id,
            monitor_size_inch: get_monitor_size_inch,
        });
    } else { // otherwise if you asked for entring manually the IDs
        pretask.push({
            type: jsPsychSurvey,
            pages: function () { // chooses which questions to ask depending on URL infos
                // creates questions
                let id_questions = [[]];
                !get_subject_id ? id_questions[0].push({
                    name: 'survey_subject_id',
                    type: 'text',
                    prompt: `Participant ID:`,
                    required: true
                }) : null;
                !get_study_id ? id_questions[0].push({
                    name: 'survey_study_id',
                    type: 'text',
                    prompt: `Study ID:`,
                    required: true
                }) : null;
                !get_session_id ? id_questions[0].push({
                    name: 'survey_session_id',
                    type: 'text',
                    prompt: `Session ID:`,
                    required: true
                }) : null;
                id_questions[0].push({
                    name: 'survey_monitor_size_inch',
                    type: 'text',
                    prompt: `What is your current screen size (inch) ?`,
                    required: true
                });
                testquestion ? id_questions[0].push({ // this question is not mandatory for the code to run, it is only informative
                    name: 'testmode',
                    type: 'text',
                    prompt: `[TEST-MODE] Enter 1 for the complete experiment (160 sustained attention trials + 80 inhibitory control trials?) or 0 for a test (2*4 trials)`,
                    required: true
                }) : null;
                return (id_questions);
            },
            on_finish: function (data) {
                // add variables to data
                get_subject_id ?
                    jsPsych.data.addProperties({ subject_id: get_subject_id })
                    : jsPsych.data.addProperties({ subject_id: data.response.survey_subject_id });
                get_study_id ?
                    jsPsych.data.addProperties({ study_id: get_study_id })
                    : jsPsych.data.addProperties({ study_id: data.response.survey_study_id });
                get_session_id ?
                    jsPsych.data.addProperties({ session_id: get_session_id })
                    : jsPsych.data.addProperties({ session_id: data.response.survey_session_id });
                jsPsych.data.addProperties({
                    monitor_size_inch: data.response.survey_monitor_size_inch,
                });
                testmode = data.response.testmode == '0' ? true : false;
            }
        });
    };

    // welcome and fullscreen mode
    pretask.push({
        type: jsPsychFullscreen,
        message: welcome_str,
        fullscreen_mode: true
    });

    // virtual chinrest
    pretask.push({
        type: jsPsychVirtualChinrest,
        blindspot_reps: 2,
        resize_units: "none",
        on_finish: function(data){
            const   stim_width_va = stim_diag_va / Math.sqrt(2); // stimulus width in va
            const   stim_width_mm = 2 * data.view_dist_mm * Math.tan(stim_width_va * Math.PI / 360); // stimulus diagonal in mm
            // const   stim_width_rad = 2 * Math.atan((stim_width_mm / 2) / (data.view_dist_mm)); // stimulus width in radian
            // const   stim_width_deg = stim_width_rad * 180 / Math.PI; // stimulus width in degrees (=VA)
            const   stim_width_px = stim_width_va * data.px2deg; // stim width in px from real monitor size in cm. Note : your screen pixel is not the same PHYSICAL size than your neighbour's pixel.
            jsPsych.data.addProperties({
                distance_cm: data.view_dist_mm/10,
                pxperdeg: data.px2deg,
                monitor_width_px: window.outerWidth,
                monitor_height_px: window.outerHeight,
                presentation_time: pres_time,
                soa: soa,
                resp_window: resp_window,
                buffer_time: buffer_time,
                stimulus_width_va: stim_width_va,
                stimulus_width_cm: stim_width_mm/10,
                stimulus_width_px: stim_width_px,
            });

            jsPsych.pauseExperiment();
            setTimeout(jsPsych.resumeExperiment, post_instructions_time);
        },
    });

    await jsPsych.run(pretask); // await allows to wait for the calibration to end

    // ##############################
    // ########## PRACTICE ##########
    // ##############################

    // ####################
    // ### instructions ###
    // ####################

    do_practice ? timeline.push({
        type: jsPsychInstructions,
        pages: [
            // 1
            `This test measures your ability to pay attention.
            You will be presented with briefly flashed displays that contain a blue square.`,
            // 2
            `If the blue square is presented at the TOP, please <span class='highlight-green'>press the spacebar</span><br><br>
            <img src='assets/img/tova_up.png' width='800' heigth='auto'></img>`,
            // 3
            `If the blue square is presented at the BOTTOM, <span class='highlight-red'>don't press the spacebar</span><br><br>
            <img src='assets/img/tova_down.png' width='800' heigth='auto'></img>`,
            // 4
            `Don't guess where the blue square will flash, make sure you see it before you press the button.<br><br>
            Try to balance speed and accuracy : press the button as fast as you can, but also try to be as accurate as you can.<br><br>
            At the end of each trial, a cross will let you know if your response was correct (green) or incorrect (red).<br><br>
            If you do make a mistake, don't worry : anyone can make a mistake on this test.`,
            // 5
            `Remember :<br><br>
            If the blue square is presented at the TOP, please press the spacebar.<br>
            If the blue square is presented at the BOTTOM, don't press the spacebar.<br>
            Please be as fast and as accurate as possible.<br><br>
            Click the button "Next" to start the practice.`
        ],
        show_clickable_nav: true,
        on_start: function() {
            document.body.style.backgroundColor = '#202020';
            document.body.style.color = '#ffffff';
        },
        on_finish: function (data) { // change color to black, hides cursor and wait post_instructions_time ms before getting to the first block
            document.body.style.backgroundColor = '#000000';
            document.body.style.cursor = 'none'; // hides cursor during task
            jsPsych.pauseExperiment();
            setTimeout(jsPsych.resumeExperiment, post_instructions_time);
        }
    }) : null;  // ternary to say to do push this in the timeline if we want to go through the practice

    // #####################
    // ### practice loop ###
    // #####################

    do_practice ? timeline.push({
        timeline: [createTovaBlock(jsPsych, 'practice', 0), createPracticeConditional(jsPsych)],
        loop_function: function() {
            if (repeat_practice[repeat_practice.length - 1] === true) { // if last practice is true, redo practice
                return true;
            } else {
                document.body.style.backgroundColor = '#202020'; // back to grey
                document.body.style.cursor = 'auto'; // display cursor during instructions
            
                // Get and clean final data
                var final = jsPsych.data.get().ignore([
                    // Select properties of trials object to get only the relevant data
                    'success',
                    'timeout',
                    'failed_images',
                    'failed_audio',
                    'failed_video',
                    'trial_index',
                    'internal_node_id',
                    'item_width_mm',
                    'item_height_mm',
                    'item_width_px',
                    'px2mm',
                    'view_dist_mm',
                    'item_width_deg',
                    'px2deg',
                    'win_width_deg',
                    'win_height_deg',
                    'view_history',
                    'response',
                    'accuracy',
                ]);
                // Filter data to get only the relevant data
                final.trials = final.trials.filter(
                    trial => trial.trial_type !== 'preload'
                    && trial.trial_type !== 'survey'
                    && trial.trial_type !== 'fullscreen'
                    && trial.trial_type !== 'virtual-chinrest'
                    && trial.trial_type !== 'instructions'
                );
                // Save data locally
                // final.localSave('csv', 'tovaPractice' + '_' + jsPsych.data.get().last(1).values()[0].subject_id + '_' + logCurrentDateTime().split('T')[0].replace(/-/g, '') + '.csv'); // BACKEND : need to save this csv , otherwise uncomment for debugging

                jsPsych.pauseExperiment();
                setTimeout(jsPsych.resumeExperiment, post_instructions_time);

                return false;
            }
        }
    }) : null;

    // ###############################
    // ########## MAIN TASK ##########
    // ###############################

    // fullscreen mode + review
    timeline.push({
        type: jsPsychFullscreen,
        message: review_str,
        fullscreen_mode: true,
        button_label: 'Begin Task',
        on_finish: function(data){ // change color to black and cursor and wait post_instructions_time ms before getting to the first block
            document.body.style.backgroundColor = '#000000'; // back to black
            document.body.style.cursor = 'none'; // hide cursor during task
            jsPsych.pauseExperiment();
            setTimeout(jsPsych.resumeExperiment, post_instructions_time);
            do_practice = false; // BACKEND should find a way to update 'do_practice' at this point, so that when a participant refreshes the window, he/she comes directly here – since the data is save before !
        }
    });

    // ####################################################################
    // ### for loop on fixed_blocks_array to create blocks and feedback ###
    // ####################################################################

    for (let i = 0; i < fixed_blocks_array.length; i++){
        // create block 
        timeline.push(createTovaBlock(jsPsych, 'main', i));

        // debrief block
        timeline.push({
            type: jsPsychHtmlKeyboardResponse,
            choices: ['Enter'],
            prompt: function () {
                const trials = jsPsych.data.get().filter({ block: block_type[i] });
                const go_trials = trials.filter({ trial_condition: 'Go' });
                const nogo_trials = trials.filter({ trial_condition: 'NoGo' });
                const correct_trials = trials.filter({ correct: 1 });
                const correct_go_trials = correct_trials.filter({ trial_condition: 'Go' });
                const correct_nogo_trials = correct_trials.filter({ trial_condition: 'NoGo' });
                const go_accuracy = Math.round(correct_go_trials.count() / go_trials.count() * 100);
                const nogo_accuracy = Math.round(correct_nogo_trials.count() / nogo_trials.count() * 100);
                const correct_go_rt = Math.round(correct_go_trials.select('rt').mean());
                if (i === fixed_blocks_array.length - 1) {
                    return `${endblock_str1}${go_accuracy}${endblock_str2}${nogo_accuracy}${endblock_str4}`;
                }
                return `${endblock_str1}${go_accuracy}${endblock_str2}${nogo_accuracy}${endblock_str3}`;
            },
            on_start: function() {
                document.body.style.backgroundColor = '#202020'; // back to grey
                document.body.style.cursor = 'auto'; // display cursor during instructions
            },
            on_finish: function() { // wait post_instructions_time ms before getting to the next block
                document.body.style.backgroundColor = '#000000'; // back to black
                document.body.style.cursor = 'none'; // hide cursor during task
                jsPsych.pauseExperiment();
                setTimeout(jsPsych.resumeExperiment, post_instructions_time);
            }
        });    
    }

    // ###################
    // ### end message ###
    // ###################

    if (savedb) {
        timeline.push({
            type: jsPsychCallFunction,
            async: true,
            func: function(done){
                // Log finish time
                jsPsych.data.addProperties({
                    log_end: logCurrentDateTime(),
                });
                // Get and clean final data
                var final = jsPsych.data.get().ignore([
                    // Select properties of trials object to get only the relevant data
                    'success',
                    'timeout',
                    'failed_images',
                    'failed_audio',
                    'failed_video',
                    'trial_index',
                    'internal_node_id',
                    'item_width_mm',
                    'item_height_mm',
                    'item_width_px',
                    'px2mm',
                    'view_dist_mm',
                    'item_width_deg',
                    'px2deg',
                    'win_width_deg',
                    'win_height_deg',
                    'view_history',
                    'response',
                    'accuracy',
                ]);
                // Filter data to get only the relevant data
                final.trials = final.trials.filter(
                    trial => trial.trial_type !== 'preload'
                    && trial.trial_type !== 'survey'
                    && trial.trial_type !== 'fullscreen'
                    && trial.trial_type !== 'virtual-chinrest'
                    && trial.trial_type !== 'instructions'
                    && trial.block !== 'instructions'
                );
                // Setup POST HTTP request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', root_path + 'src/save.php');
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.onload = function() {
                if(xhr.status == 200){
                    var response = xhr.responseText;
                    console.log(response);
                }
                done(); // invoking done() causes experiment to progress to next trial.
                };
                xhr.send(final.json());
            }
        });
    }

    timeline.push({
        type: jsPsychInstructions,
        pages: [
            classic_end_str,
            // or it can be
            // completion_code_str
            // inlab_final_str
        ],
        show_clickable_nav: true,
        allow_backward: false,
        button_label_next: "Finish Experiment",
        on_start: function () {
            // Show cursor
            document.body.style.cursor = "auto";
        
            // Exit fullscreen
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) { /* Safari */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE11 */
                document.msExitFullscreen();
            }
            window.onbeforeunload = null; // disable the prevention
        },
        on_finish: function () {
            // when finished and button not shown, redirect to tasks
            if (showcsvbutton == false) {
                const p = document.createElement("p");
                p.setAttribute("style","position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 16px; color: white;");
                p.innerText = redirectText;
                document.body.appendChild(p);
                setTimeout(() => {
                    window.location.replace(redirectURL);
                    console.log(redirectText)
                }, 3000);
                return;
            }
            // Log finish time
            jsPsych.data.addProperties({
                logEnd: logCurrentDateTime(),
            });
            // Get and clean final data
            var final = jsPsych.data.get().ignore([
                // Select properties of trials object to get only the relevant data
                'success',
                'timeout',
                'failed_images',
                'failed_audio',
                'failed_video',
                'trial_index',
                'internal_node_id',
                'item_width_mm',
                'item_height_mm',
                'item_width_px',
                'px2mm',
                'view_dist_mm',
                'item_width_deg',
                'px2deg',
                'win_width_deg',
                'win_height_deg',
                'view_history',
                'response',
                'accuracy',
            ]);
            // Filter data to get only the relevant data
            final.trials = final.trials.filter(
                trial => trial.trial_type !== 'preload'
                && trial.trial_type !== 'survey'
                && trial.trial_type !== 'fullscreen'
                && trial.trial_type !== 'virtual-chinrest'
                && trial.trial_type !== 'instructions'
                && trial.block !== 'instructions'
            );
            console.log(final);
            // Save data locally
            final.localSave('csv', 'tova' + '_' + jsPsych.data.get().last(1).values()[0].subject_id + '_' + logCurrentDateTime().split('T')[0].replace(/-/g, '') + '.csv'); // BACKEND : need to save this csv , otherwise uncomment for debugging

            // Backup button in case participant goes too fast
            const button = document.createElement("button");
            button.innerText = "Download CSV";
            button.id = "downloadCsvButton";
            button.style.cssText = "position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); padding: 10px 20px; font-size: 16px; background-color: #4CAF50; color: white; border: none; border-radius: 5px; cursor: pointer;";
            // Append the button to the body or any other element in the document
            document.body.appendChild(button);
            // Add the event listener to trigger final.localSave() on button click
            button.addEventListener("click", function () {
                if (typeof final !== "undefined" && typeof final.localSave === "function") {
                    final.localSave('csv', 'tova' + '_' + jsPsych.data.get().last(1).values()[0].subject_id + '_' + logCurrentDateTime().split('T')[0].replace(/-/g, '') + '.csv'); // BACKEND : need to save this csv , otherwise uncomment for debugging
                } else {
                    console.error("final.localSave() is not defined.");
                }
            });
        }
    });

    jsPsych.run(timeline); 

};

tova();