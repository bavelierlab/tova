/* db-structure.sql:
 * This is the structure of the database we used, which is compatible with the current code.
 * You may make modifications, but you will need to update the code used for the tasks accordingly.
 */

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

-- CREATE TABLE IF NOT EXISTS `subjects` (
--   `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
--   `username` varchar(50) DEFAULT NULL,
--   `creationTime` datetime DEFAULT NULL,
--   `locTime` datetime DEFAULT NULL,
--   PRIMARY KEY (`sid`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tova`
--

CREATE TABLE IF NOT EXISTS `tova` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `subject_id` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
    `study_id` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
    `session_id` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
    `log_start` datetime DEFAULT NULL,
    `log_end` datetime DEFAULT NULL,
    `browser` varchar(15) DEFAULT NULL,
    `browser_version` varchar(15) DEFAULT NULL,
    `mobile` varchar(5) DEFAULT NULL,
    `os` varchar(15) DEFAULT NULL,
    `fullscreen` varchar(5) DEFAULT NULL,
    `vsync_rate` double DEFAULT NULL,
    `monitor_size_inch` double DEFAULT NULL,
    `distance_cm` double DEFAULT NULL,
    `pxperdeg` double DEFAULT NULL,
    `monitor_width_px` int(11) DEFAULT NULL,
    `monitor_height_px` int(11) DEFAULT NULL,
    `presentation_time` int(11) DEFAULT NULL,
    `soa` int(11) DEFAULT NULL,
    `resp_window` int(11) DEFAULT NULL,
    `buffer_time` int(11) DEFAULT NULL,
    `stimulus_width_va` double DEFAULT NULL,
    `stimulus_width_cm` double DEFAULT NULL,
    `stimulus_width_px` double DEFAULT NULL,
    `trial_type` varchar(30) DEFAULT NULL,
    `real_trial_index` int(11) DEFAULT NULL,
    `block` varchar(11) DEFAULT NULL,
    `time_elapsed` double DEFAULT NULL,
    `rt` int(11) DEFAULT NULL,
    `stimulus` varchar(30) DEFAULT NULL,
    `trial_condition` varchar(5) DEFAULT NULL,
    `position_is_top` tinyint(4) DEFAULT NULL,
    `effective_response` tinyint(4) DEFAULT NULL,
    `correct` tinyint(4) DEFAULT NULL,
    `trial_accuracy` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;